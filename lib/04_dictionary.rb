class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(input)
    if input.is_a? String
      @entries[input] = nil
    else
      input.each {|keyword, definition| @entries[keyword] = definition}
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.has_key?(key)
  end

  def find(prefix)
    result = {}
    @entries.each do |word, definition|
      result[word] = definition if word.index(prefix) == 0
    end
    result
  end

  def printable
    output = ""
    self.keywords.each do |keyword|
      output += "[#{keyword}] \"#{entries[keyword]}\"\n"
    end
    output.strip
  end
end
