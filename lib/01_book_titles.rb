class Book

  attr_accessor :title

  def title=(title)
    dont_capitalize = ["in","the","a","an","and","of","but","or","nor","yet","for","so"]
    new_title = []
    title.split.each_with_index do |word, idx|
      if dont_capitalize.include?(word) && idx != 0
        new_title << word
      else
        new_title << word.capitalize
      end
    end
    @title = new_title.join(" ")
  end
end
