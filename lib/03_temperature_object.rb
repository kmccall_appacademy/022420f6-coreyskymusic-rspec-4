class Temperature
  def initialize(temp_hash)
    temp_hash.key?(:f) ? @fahrenheit = temp_hash[:f] : @celsius = temp_hash[:c] 
  end

  def in_fahrenheit
    @fahrenheit ? @fahrenheit : @fahrenheit = Temperature.ctof(@celsius)
  end

  def in_celsius
    @celsius ? @celsius : @celsius = Temperature.ftoc(@fahrenheit)
  end

  def self.from_celsius(temp)
    temp_hash = { c: temp }
    Temperature.new(temp_hash)
  end

  def self.from_fahrenheit(temp)
    temp_hash = { f: temp }
    Temperature.new(temp_hash)
  end

  def self.ftoc(f_temp)
    (f_temp - 32) * (5.0 / 9.0)
  end

  def self.ctof(c_temp)
    (c_temp.to_f / (5.0 / 9.0)) + 32
  end

end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
