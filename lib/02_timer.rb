class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 60 / 60
    minutes = @seconds / 60 % 60
    secs = @seconds % 60 % 60
    time = [hours, minutes, secs]
    @time_string = time.map { |value| padded(value) }.join(":")
  end

  def padded(value)
    value < 10 ? "0" + value.to_s : value.to_s
  end

end
